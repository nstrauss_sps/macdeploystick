#include <HID-Project.h>
#include <HID-Settings.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <avr/wdt.h> 
#include "ArduinoJson.h"

#define CURRENTVERSION 4
#define DEFAULTCOMMAND "/Volumes/MacDeployStick/run"
struct settings_t
{
  int version;
  char command[255];
  long startup_delay;
} settings;

void reboot() {
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void setup() {

}
void showusage(){
  Serial.println();
  Serial.println("Copyright 2018 Twocanoes Software, Inc.");
  Serial.println("help: this message");
  Serial.println("show: show current settings");
  Serial.println("reset: reset settings to defaults");
  Serial.println("reboot: reboot the device");
  Serial.println("set_command <command>: set command to run in recovery.");
  Serial.println("set_startup_delay <seconds>: how many seconds to wait from booting in recovery to launching terminal.");
  Serial.println("set_settings <json>: Provide all settings in JSON format.");
  Serial.println("get_settings: Get all settings in JSON format.");

  Serial.println();
  
}

void setdefaults(){
  strcpy(settings.command, DEFAULTCOMMAND);
  settings.version=CURRENTVERSION;
  settings.startup_delay=120;
  EEPROM_writeAnything(0, settings);
  EEPROM_readAnything(0, settings);

}
void loop() {
    delay(500);  
    Serial.begin(9600); 
     delay(2000); 
    EEPROM_readAnything(0, settings);
    if (settings.version != CURRENTVERSION) {
        setdefaults();
    }
    Serial.println("Copyright 2018 Twocanoes Software, Inc."); 
    Serial.println("Press <return> to enter configuration mode.");
    int i;
    bool cli=false;
    for (i=0;i<10;i++) {
        if(Serial.available()) {
          while(Serial.available()) {
            Serial.read();
          }
          cli=true;
          break;
        }
        else {
           delay(500);
        }
    }
    if (cli) {
          Serial.println("Configuration Mode. Enter help for assistance. Copyright 2018 Twocanoes Software, Inc."); 
          while(1) {
            Serial.setTimeout(100000);
            
            Serial.print(">");
            String s = Serial.readStringUntil('\n');
            s.trim();
            if (s.compareTo("show")==0) {

              
              Serial.print("Version: ");
              Serial.println(settings.version,DEC);
              Serial.print("Command:");
              Serial.println(settings.command); 
              Serial.print("Startup Delay:");
              Serial.println(settings.startup_delay,DEC);
              
            }
            else if (s.startsWith("set_command")==true) {

              String new_command=s.substring(12);
              strncpy(settings.command,new_command.c_str(),255);
               EEPROM_writeAnything(0, settings);
              
            }
             else if (s.startsWith("help")==true) {
                showusage();
                
                    
              
            }
            else if (s.startsWith("set_startup_delay")==true) {
              String new_command=s.substring(18);
              settings.startup_delay=new_command.toInt();
               EEPROM_writeAnything(0, settings);
              
            }
           else if (s.startsWith("reset")==true) {
              setdefaults();
           }
          else if (s.startsWith("reboot")==true) {

              reboot();              
            }
            else if (s.length()==0) {


            }
            else if (s.startsWith("set_settings")==true) {
                
                StaticJsonBuffer<200> jsonBuffer;
                String json=s.substring(13);
                JsonObject& root = jsonBuffer.parseObject(json);
                
                const char *command = root["command"];
                strncpy(settings.command,command,255);
                settings.startup_delay  =  root["startup_delay"];
                EEPROM_writeAnything(0, settings);
            }
            else if (s.startsWith("get_settings")==true) {
                StaticJsonBuffer<200> jsonBuffer;
                
                JsonObject& root = jsonBuffer.createObject();
                root["command"] = settings.command;
                root["startup_delay"] = settings.startup_delay;
                root["version"] = settings.version;
          
                
                root.printTo(Serial);
                Serial.println();   
            }
            else {
              Serial.println("Invalid command. Enter help for usage.");
             
            }
          }
          
      }
 

  Serial.println("Running...");
Serial.end();
  BootKeyboard.begin();
  delay(500);
  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press('r');
  delay(5000);
  BootKeyboard.releaseAll();
  
  long total_delay=settings.startup_delay*1000;
  delay(total_delay);

  BootKeyboard.press(KEY_ESC);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_ESC);
  delay(500);
  BootKeyboard.releaseAll();
  delay(500);
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();

  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);

  BootKeyboard.releaseAll();
  BootKeyboard.println("\n");

  delay(5500);
  BootKeyboard.println(settings.command);

  while (1);
}
