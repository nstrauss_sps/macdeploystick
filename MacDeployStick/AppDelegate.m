//
//  AppDelegate.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//
//#define BETA 1

#import "AppDelegate.h"
#import "DeploySettings.h"
#import <TCSPromo/TCSPromoManager.h>

@interface AppDelegate ()
@property (assign) BOOL isStoreVersion;
@end

@implementation AppDelegate
#ifndef APPSTORE
- (nullable PADDisplayConfiguration *)willShowPaddleUIType:(PADUIType)uiType product:(nonnull PADProduct *)product{

    PADDisplayConfiguration *config=[[PADDisplayConfiguration alloc] initWithDisplayType:PADDisplayTypeSheet hideNavigationButtons:NO parentWindow:[NSApp mainWindow]];
    return config;

}
-(void)showPaddleInfo{
    NSString *myPaddleProductID = @"548513";
    NSString *myPaddleVendorID = @"16588";
    NSString *myPaddleAPIKey = @"d20e1996121f529dac694ebddb57a294";

    // Default Product Config in case we're unable to reach our servers on first run:
    PADProductConfiguration *defaultProductConfig = [[PADProductConfiguration alloc] init];

    defaultProductConfig.productName = @"Boot Camp ISO Converter";
    defaultProductConfig.vendorName = @"Twocanoes Software, Inc";

    // Initialize the SDK singleton with the config:
    Paddle *paddle = [Paddle sharedInstanceWithVendorID:myPaddleVendorID
                                                 apiKey:myPaddleAPIKey
                                              productID:myPaddleProductID
                                          configuration:defaultProductConfig];
    paddle.delegate=self;
    // Initialize the Product you'd like to work with:
    PADProduct *paddleProduct = [[PADProduct alloc] initWithProductID:myPaddleProductID productType:PADProductTypeSDKProduct configuration:nil];


    // Ask the Product to get it's latest state and info from the Paddle Platform:
    [paddleProduct refresh:^(NSDictionary * _Nullable productDelta, NSError * _Nullable error) {
        // Optionally show the default "Product Access" UI to gatekeep your app
        [paddle showProductAccessDialogWithProduct:paddleProduct];
    }];


}
#endif
-(BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender{
    return YES;

}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {


#ifdef APPSTORE
        self.isStoreVersion=YES;
#else
    self.isStoreVersion=NO;
    [self showPaddleInfo];
#endif
    NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

    if (lastTimePromoShown==nil) {

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"]; lastTimePromoShown=[NSDate date]; }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO && (fabs([lastTimePromoShown timeIntervalSinceNow])>60*60*24*7) ) {
        [[TCSPromoManager sharedPromoManager] showPromoWindow:self];

        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
    }


#ifdef BETA

    NSString *compileDate = [NSString stringWithUTF8String:__DATE__];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MMM d yyyy"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [df setLocale:usLocale];
    NSDate *betaExpireDate = [[df dateFromString:compileDate] dateByAddingTimeInterval:60*60*24*14];

    NSLog(NSLocalizedString(@"Beta Product.  Expires on %@",@"log message"),[betaExpireDate description]);


    NSTimeInterval ti=[betaExpireDate timeIntervalSinceNow];
    NSInteger res;
    if (ti<0) {
        res=NSRunAlertPanel(NSLocalizedString(@"Beta Period Ended",@"Title for alert panel that beta expired"),NSLocalizedString(@"This beta has expired.  Please visit twocanoes.com to download an updated version.",@"body for alert panel that beta expired"), NSLocalizedString(@"Visit",@"button alert panel that beta expired"),NSLocalizedString(@"Quit", @"button for alert panel that beta expired"),nil);
        if (res==NSAlertDefaultReturn) {
            ;
            NSURL *url=[NSURL URLWithString:SUPPORTURLS[@"mainwebsite"]];
            [[NSWorkspace sharedWorkspace]  openURL:url];
        }
        [NSApp terminate:self];
    }
    else {

        NSAlert *alert=[NSAlert alertWithMessageText:@"Beta Product" defaultButton:@"OK" alternateButton:nil otherButton:nil informativeTextWithFormat:@"This is a beta product and will expire on %@",[betaExpireDate description]];

        [alert runModal];


    }


#endif
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {

    [[DeploySettings sharedSettings] saveToPrefs:self];
}


@end
