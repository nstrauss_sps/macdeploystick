 //
//  TCSCopyFileController.m
//  Winclone
//
//  Created by Tim Perfitt on 5/5/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import "TCSCopyFileController.h"
FSFileOperationRef fileOp;

static void statusCallback (FSFileOperationRef fileOp,
                            const FSRef *currentItem,
                            FSFileOperationStage stage,
                            OSStatus error,
                            CFDictionaryRef statusDictionary,
                            void *info
                            )
{
   
    id delegate;
    if (info)
        delegate = (__bridge id)info;

    if (stage == kFSOperationStageComplete && delegate && error==0) {
        [delegate copyCompleted:delegate withError:nil];
        
        
        return;
    }
    
    if (error!=0) {
        
        NSError *err=[NSError errorWithDomain:@"TCS" code:1 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Error copying file. Check permissions and free space in the location where the package is being saved and then try creating the package again. (Error:%d)", (int)error]}];
        if (delegate) {
            [delegate copyCompleted:delegate withError:err];
            
            
            return;
        }
        
    }
    if (statusDictionary)
    {
        CFNumberRef totalBytes, bytesCompleted;
        
        bytesCompleted = (CFNumberRef) CFDictionaryGetValue(statusDictionary, kFSOperationBytesCompleteKey);
        
        totalBytes = (CFNumberRef) CFDictionaryGetValue(statusDictionary, kFSOperationTotalBytesKey);
        
        if (totalBytes && bytesCompleted) {
            
            float tb;
            CFNumberGetValue (
                              totalBytes,
                              kCFNumberFloat32Type,
                              &tb
                              );
            
            float bc;
            CFNumberGetValue (
                              bytesCompleted,
                              kCFNumberFloat32Type,
                              &bc
                              );
            
            
            
            
            int completed=roundf((bc/tb)*100.0);
            
            if (delegate){
                [delegate percentCompleted:completed];
            }
        }
        
        
        
    }
}
@interface TCSCopyFileController()
@end


@implementation TCSCopyFileController

- (void)copyFile:(NSString *)sourceFilePath toPath:(NSString *)destinationFilePath delegate:(id)inDelegate
{
    

    CFRunLoopRef runLoop = CFRunLoopGetCurrent();
    fileOp = FSFileOperationCreate(kCFAllocatorDefault);
    
    OSStatus status2 = FSFileOperationScheduleWithRunLoop(fileOp, runLoop, kCFRunLoopDefaultMode);
    if( status2 )
    {
        NSLog(@"Failed to schedule operation with run loop: %i", status2);
        return;
    }
    

    
    FSRef source;
    FSRef destination;
    
    FSPathMakeRef( (const UInt8 *)[sourceFilePath cStringUsingEncoding:NSUTF8StringEncoding], &source, NULL );
    Boolean isDir = true;
    FSPathMakeRef( (const UInt8 *)[destinationFilePath  cStringUsingEncoding:NSUTF8StringEncoding], &destination, &isDir );
    
    
    FSFileOperationClientContext    clientContext;
    
    
    // The FSFileOperation will copy the data from the passed in clientContext so using
    // a stack based record that goes out of scope during the operation is fine.
    if (inDelegate)
    {
        clientContext.version = 0;
        clientContext.info = (__bridge void *) inDelegate;
        clientContext.retain = CFRetain;
        clientContext.release = CFRelease;
        clientContext.copyDescription = CFCopyDescription;
    }
    
    OSStatus status = FSCopyObjectAsync (fileOp,
                                         &source,
                                         &destination,
                                         NULL,
                                         kFSFileOperationDefaultOptions,
                                         statusCallback,
                                         0.3,
                                         inDelegate != NULL ? &clientContext : NULL);
    
    CFRelease(fileOp);
    
    if( status )
        NSLog(@"Status: %i", status);
    
}
-(int)cancelCopyOperation{
    FSFileOperationCancel ( fileOp);
    NSFileManager *fm=[NSFileManager defaultManager];
    NSError *err;
    if (self.delegate) [self.delegate copyCompleted:self withError:
                        [NSError errorWithDomain:@"TCS" code:-2 userInfo:@{NSLocalizedDescriptionKey:@"Copy Cancelled"}]];

    if ([fm removeItemAtPath:self.sourceFilePath error:&err]==NO) {
                [self.delegate copyCompleted:self withError:
         [NSError errorWithDomain:@"TCS" code:-1 userInfo:@{@"ErrorMessage":@"Could not remove package"}]];
        return -1;
        
    }
    
    return 0;
}
@end
