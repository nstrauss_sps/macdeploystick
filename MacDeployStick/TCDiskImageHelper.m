//
//  TCDiskImageHelper.m
//
//  Created by Timothy Perfitt on 3/1/12.
//  Copyright (c) 2012 Twocanoes Software Inc. All rights reserved.
//

#import "TCDiskImageHelper.h"
#import "TCTaskHelper.h"
@interface TCDiskImageHelper()
- (void)appendOutput:(id)output;
- (void)processStarted;
- (void)processFinished;

@end
@implementation TCDiskImageHelper
@synthesize deviceInfo;
-(NSString *)createdDiskImageWithSize:(NSString *)inSize filename:(NSString *)inFilename volumeName:(NSString *)inVolName{


    NSString *filepath=inFilename;
    NSArray *options;

    options=@[@"create",@"-fs",@"JHFS+",@"-volname",inVolName,@"-size",inSize,filepath];

    [[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/bin/hdiutil" withOptions:options];


    NSString *output=[[TCTaskHelper sharedTaskHelper] runCommand:@"/usr/bin/hdiutil" withOptions:@[@"attach",filepath,@"-plist"]];

    NSData *plistData = [output dataUsingEncoding:NSUTF8StringEncoding];

    NSString *error2;
    NSPropertyListFormat format;
    NSDictionary *plist;

    plist = [NSPropertyListSerialization propertyListFromData:plistData
                                             mutabilityOption:NSPropertyListImmutable
                                                       format:&format
                                             errorDescription:&error2];


    __block NSString *device;
    [[plist objectForKey:@"system-entities"] enumerateObjectsUsingBlock:^(NSDictionary *info, NSUInteger idx, BOOL * _Nonnull stop) {

        if ([[info objectForKey:@"content-hint"] isEqualToString:@"FDisk_partition_scheme"] ||
            [[info objectForKey:@"content-hint"] isEqualToString:@"GUID_partition_scheme"]) {

            device=[info objectForKey:@"dev-entry"];
            *stop=YES;

        }
    }];

    return device;
}
-(void)attachDiskImageAtPath:(NSString *)inPath withDelegate:(id)sender{
    operation=MOUNTING;
    self.deviceInfo=[NSMutableString string];
    delegate=sender;
    NSArray *executableArray=[NSArray arrayWithObjects:@"/usr/bin/hdiutil",@"attach",@"-nomount",inPath,nil];

    tw=[[TaskWrapper alloc] initWithController:self arguments:executableArray outputTypeBinary:NO];
    [tw startProcess];

    
}




-(void)unmountDiskImage:(NSString *)inPath withDelegate:(id)sender{
    operation=UNMOUNTING;
    self.deviceInfo=[NSMutableString string];
    delegate=sender;
    NSArray *executableArray=[NSArray arrayWithObjects:@"/usr/bin/hdiutil",@"detach",inPath,nil];
    
    tw=[[TaskWrapper alloc] initWithController:self arguments:executableArray outputTypeBinary:NO];
    [tw startProcess];
    

}
- (void)appendOutput:(id)output{
    [self.deviceInfo appendString:output];
}

// This method is a callback which your controller can use to do other initialization when a process
// is launched.
- (void)processStarted{
}

// This method is a callback which your controller can use to do other cleanup when a process
// is halted.
- (void)processFinished{
    if (operation==MOUNTING) {
        //self.deviceInfo

        NSString *device=[[self.deviceInfo componentsSeparatedByString:@" "] objectAtIndex:0];
            
        if ([delegate respondsToSelector:@selector(diskImageMountedAtDevice:)]){
            [delegate diskImageMountedAtDevice:device];
        }
    }
    else if ([delegate respondsToSelector:@selector(diskImageUnMounted)]){
        [delegate diskImageUnMounted];
    }
}

@end


