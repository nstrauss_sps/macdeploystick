//
//  DeploySettings.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "DeploySettings.h"
@interface DeploySettings()

@end
@implementation DeploySettings

+ (id)sharedSettings {
    static DeploySettings *sharedDeploySettings = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"workflowInfo"]){

            NSError *err;
            NSData *inData=[[NSUserDefaults standardUserDefaults] objectForKey:@"workflowInfo"];
            NSSet *set = [NSSet setWithArray:@[
                                               [NSArray class],
                                               [DeploySettings class],
                                               [TCSWorkflow class],
                                               [TCSWorkflowLocalization class],
                                                [NSURL class],
                                               [NSData class],
                                               ]];


            sharedDeploySettings = [NSKeyedUnarchiver unarchivedObjectOfClasses:set fromData:inData error: &err];



        }
        else sharedDeploySettings = [[self alloc] init];

    });
    BOOL isStale;
    NSError *err;
    NSURL *installMediaBookmarkResolved=[NSURL URLByResolvingBookmarkData:[sharedDeploySettings installMediaBookmark] options:NSURLBookmarkResolutionWithSecurityScope relativeToURL:nil bookmarkDataIsStale:&isStale error:&err];

    [installMediaBookmarkResolved startAccessingSecurityScopedResource];

    return sharedDeploySettings;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}
- (id)init {
    if (self = [super init]) {
        self.workflows=[NSMutableArray array];

    }
    return self;

}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.backgroundImageURL = [decoder decodeObjectForKey:@"backgroundImageURL"];
    self.installMediaURL = [decoder decodeObjectForKey:@"installMediaURL"];
    self.installMediaBookmark = [decoder decodeObjectForKey:@"installMediaBookmark"];
    self.workflows = [decoder decodeObjectForKey:@"workflows"];
    self.shouldSaveToDMG=NO;

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.backgroundImageURL forKey:@"backgroundImageURL"];
    [encoder encodeObject:self.installMediaURL forKey:@"installMediaURL"];
    [encoder encodeObject:self.installMediaBookmark forKey:@"installMediaBookmark"];

    [encoder encodeObject:self.workflows forKey:@"workflows"];
}
-(void)addWorkflow:(TCSWorkflow *)workflow{
    NSMutableArray *proxyArray=[self mutableArrayValueForKey:@"workflows"];
    [proxyArray addObject:workflow];

}
-(void)saveToPrefs:(id)sender{
    NSError *err;
    NSData *dataToSave=[NSKeyedArchiver archivedDataWithRootObject:self requiringSecureCoding:YES error:&err];

    NSLog(@"error :%@",err);
    [[NSUserDefaults standardUserDefaults] setObject:dataToSave forKey:@"workflowInfo"];
}
-(BOOL)saveResourcesToURL:(NSURL *)inResourceURL withPrefix:(NSURL * )inPrefix error:(NSError **)error{
    NSFileManager *fm=[NSFileManager defaultManager];
    self.stopCopying=NO;
   __block NSError *err;

    NSURL *appURL=[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Applications"];



    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *currWorkflow, NSUInteger idx, BOOL * _Nonnull workflowsStop) {


        NSURL *packagesURL=[[[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:currWorkflow.workflowName] URLByAppendingPathComponent:@"Packages"];

        NSLog(@"copying packages");
        if (![fm fileExistsAtPath:packagesURL.path]){
            if([fm createDirectoryAtPath:packagesURL.path withIntermediateDirectories:YES attributes:nil error:&err]==NO) {
                *workflowsStop=YES;
            }
        }

        if (*workflowsStop==NO) {
            NSURL *bookmarkedURL;
            if (currWorkflow.packagesFolderBookmark) {
                BOOL isStale;
                NSError *err;
                bookmarkedURL=[NSURL URLByResolvingBookmarkData:currWorkflow.packagesFolderBookmark options:NSURLBookmarkResolutionWithSecurityScope relativeToURL:nil bookmarkDataIsStale:&isStale error:&err];
                [bookmarkedURL startAccessingSecurityScopedResource];

            }
            [[self packagesPathsInFolder:currWorkflow.packagesFolderPath] enumerateObjectsUsingBlock:^(NSString *currPackageName, NSUInteger idx, BOOL * _Nonnull stop) {

                NSString *fullPackagePathSource=[currWorkflow.packagesFolderPath stringByAppendingPathComponent:currPackageName];

                NSString *fullPackagePathDestination=[packagesURL.path stringByAppendingPathComponent:currPackageName];

                if ([fm fileExistsAtPath:fullPackagePathDestination]==NO) {

                    NSPipe *stdOut=[NSPipe pipe];
                    NSTask *packageInfoTask=[[NSTask alloc] init];
                    packageInfoTask.launchPath=@"/usr/sbin/installer";
                    packageInfoTask.arguments=@[@"-pkg",fullPackagePathSource,@"-pkginfo",@"-verbose",@"-plist"];
                    packageInfoTask.standardOutput=stdOut;
                    [packageInfoTask launch];
                    NSData *pkgInfoData=[[stdOut fileHandleForReading] readDataToEndOfFile];
                    if (pkgInfoData) {
                        NSDictionary * dict = [NSPropertyListSerialization propertyListWithData:pkgInfoData options:NSPropertyListImmutable format:nil error:&err];

                        if (err) {
                            *stop=YES;
                        }

                        if ([dict objectForKey:@"Status"] && [[dict objectForKey:@"Status"] isEqualToString:@"MetaPackage"]) {
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",fullPackagePathSource.lastPathComponent]}];

                            if ([fm copyItemAtPath:fullPackagePathSource toPath:fullPackagePathDestination error:&err]==NO) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];
                            }




                        }
                        else {

                            NSTask *productBuildTask=[[NSTask alloc] init];
                            productBuildTask.launchPath=@"/usr/bin/productbuild";
                            productBuildTask.arguments=@[@"--package",fullPackagePathSource,fullPackagePathDestination];
                            [productBuildTask launch];
                            [productBuildTask waitUntilExit];



                        }
                    }

                }

                if (self.stopCopying==YES) *stop=YES;
            }];

            if ([self packagesPathsInFolder:currWorkflow.packagesFolderPath].count>0) {
                //need to add the cleanup package
                NSString *cleanupPackage=[[NSBundle mainBundle] pathForResource:@"TCSCleanup" ofType:@"pkg"];
                if (cleanupPackage) {
                    NSString *fullCleanupPathDestination=[packagesURL.path stringByAppendingPathComponent:@"TCSCleanup.pkg"];

                    if ([fm fileExistsAtPath:fullCleanupPathDestination]==NO) {

                        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",cleanupPackage.lastPathComponent]}];



                        if ([fm copyItemAtPath:cleanupPackage toPath:fullCleanupPathDestination error:&err]==NO){
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];
                        }

                    }




                }


            }
            [bookmarkedURL stopAccessingSecurityScopedResource];

            if (err) *workflowsStop=YES;
        }
        if (self.stopCopying==YES) *workflowsStop=YES;

    }];
    if (err) {
        *error=err;
        return NO;
    }
    if (self.stopCopying==YES) return NO;
    if (![fm fileExistsAtPath:appURL.path]){
        if([fm createDirectoryAtPath:appURL.path withIntermediateDirectories:YES attributes:nil error:&err]==NO ){
            *error=err;
            return NO;
        }
    }
    NSLog(@"copying Imagr.app");
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    NSString *sourceImagrPath=[appSupportDir stringByAppendingPathComponent:@"Imagr.app"];

    if (![fm fileExistsAtPath:sourceImagrPath]){
        NSLog(@"%@ does not exist!",sourceImagrPath);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":[NSError errorWithDomain:@"TCS" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"imagr app does not exist"}]}];

        return NO;
    }

    NSString *destinationImagrPath=[appURL.path stringByAppendingPathComponent:@"Imagr.app"];

    if (![fm fileExistsAtPath:destinationImagrPath]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceImagrPath.lastPathComponent]}];

        if([fm copyItemAtPath:sourceImagrPath toPath:destinationImagrPath error:&err]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];

        }




    }
    if (self.stopCopying==YES) return NO;

    if (![fm fileExistsAtPath:destinationImagrPath]) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceImagrPath.lastPathComponent]}];

        if([fm copyItemAtPath:sourceImagrPath toPath:destinationImagrPath error:&err]==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];

        }

    }


    NSString *sourceRunPath=[[NSBundle mainBundle] pathForResource:@"run" ofType:@""];
    NSString *destinationRunPath=[inResourceURL.path stringByAppendingPathComponent:@"run"];

    if ([fm fileExistsAtPath:destinationRunPath]==YES) {

        if([fm removeItemAtPath:destinationRunPath error:&err]==NO) {
            *error=err;
            return NO;

        }

    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyItemStatus" object:self userInfo:@{@"message":[NSString stringWithFormat:@"Copying %@",sourceRunPath.lastPathComponent]}];

    if([fm copyItemAtPath:sourceRunPath toPath:destinationRunPath error:&err]==NO) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":err}];

    }
    if (self.stopCopying==YES) return NO;

    if([fm setAttributes:@{NSFilePosixPermissions:[NSNumber numberWithShort:0755]} ofItemAtPath:destinationRunPath error:&err]==NO) {

    }



    NSURL *prefix=[inPrefix URLByAppendingPathComponent:@"Deploy"];
    if (!prefix) prefix=[inResourceURL URLByAppendingPathComponent:@"Deploy"];

    NSURL *config=[[inResourceURL URLByAppendingPathComponent:@"Deploy"] URLByAppendingPathComponent:@"Config"];
    
    NSURL *serverConfigURL=[[prefix URLByAppendingPathComponent:@"Config"] URLByAppendingPathComponent:@"config.plist"];
    NSDictionary *configDict=@{@"serverurl":[serverConfigURL description]};
    NSString *destinationConfigPath=[appURL URLByAppendingPathComponent:@"com.grahamgilbert.Imagr.plist"].path;



    if ([fm fileExistsAtPath:destinationConfigPath]) {

        if ([fm removeItemAtPath:destinationConfigPath error:&err]==NO) {
            *error=err;
            return NO;
        }

    }
    if([configDict writeToFile:destinationConfigPath atomically:NO]==NO) {
        return NO;
    }

    if (self.stopCopying==YES) return NO;

    //config





//hdiutil create -srcfolder ~/Desktop/Install\ macOS\ Mojave.app ~/Desktop/Install_macOS_Mojave.dmg

    

    NSDictionary *settingsDict=[self imagrDictionaryWithResourcePrefix:prefix];

    if (![fm fileExistsAtPath:config.path]){
        if([fm createDirectoryAtPath:config.path withIntermediateDirectories:YES attributes:nil error:&err]==NO ){
            *error=err;
            return NO;
        }
    }

    NSString *destinationWorkflowConfigPath=[[config URLByAppendingPathComponent:@"config.plist"] path];

    [settingsDict writeToFile:destinationWorkflowConfigPath atomically:NO];

    return YES;
}
-(void)replaceWorkflow:(TCSWorkflow *)oldWorkflow withWorkflow:(TCSWorkflow *)workflow{
    NSMutableArray *proxyArray=[self mutableArrayValueForKey:@"workflows"];

    NSInteger index=[proxyArray indexOfObjectIdenticalTo:oldWorkflow];
    if (index!=NSNotFound) {
        [proxyArray replaceObjectAtIndex:index withObject:workflow];


    }
    else {
        [proxyArray addObject:workflow];

    }
}

-(NSDictionary *)imagrDictionaryWithResourcePrefix:(NSURL  *)resourcePrefix{



//    NSURL *scriptsURL=[resourcePrefix URLByAppendingPathComponent:@"Scripts"];

    NSURL *OSURL=[resourcePrefix URLByAppendingPathComponent:@"macOS"];

    NSMutableDictionary *imagrDict=[NSMutableDictionary dictionary];

//    if (self.backgroundImageURL) {
//        imagrDict[@"background_image"]=[[resourcesURL URLByAppendingPathComponent:self.backgroundImageURL.lastPathComponent] description];
//
//    }

    NSMutableArray *imagrWorkflows=[NSMutableArray array];

    [self.workflows enumerateObjectsUsingBlock:^(TCSWorkflow *workflow, NSUInteger idx, BOOL * _Nonnull stop) {

        NSURL *bookmarkedURL;
        if (workflow.packagesFolderBookmark) {
            BOOL isStale;
            NSError *err;
            bookmarkedURL=[NSURL URLByResolvingBookmarkData:workflow.packagesFolderBookmark options:NSURLBookmarkResolutionWithSecurityScope relativeToURL:nil bookmarkDataIsStale:&isStale error:&err];
            [bookmarkedURL startAccessingSecurityScopedResource];
        }

        NSMutableDictionary *imagrWorkflow=[NSMutableDictionary dictionary];
        if (workflow.isActive==YES) {

            imagrWorkflow[@"name"]=workflow.workflowName;
            imagrWorkflow[@"description"]=workflow.workflowDescription;
            imagrWorkflow[@"bless_target"]=[NSNumber numberWithBool:workflow.shouldBlessTarget];

            NSMutableArray *imagrComponents=[NSMutableArray array];

//            if (workflow.useRestartAction==YES){
//                switch (workflow.restartAction) {
//                    case Restart:
//                        imagrWorkflow[@"restart_action"]=@"restart";
//
//                        break;
//                    case Shutdown:
//                        imagrWorkflow[@"restart_action"]=@"shutdown";
//                        break;
//                    case None:
//                        imagrWorkflow[@"restart_action"]=@"none";
//                        break;
//
//                    default:
//                        break;
//                }
//
//            }
//
//            if (workflow.useComputerName==YES ) {
//
//                switch (workflow.computerName) {
//                    case ComputerNameAsk:
//                        [imagrComponents addObject:@{@"type":@"computer_name"}];
//
//
//                        break;
//                    case ComputerNameSerialAsk:
//                        [imagrComponents addObject:@{@"type":@"computer_name",
//                                                     @"use_serial":@YES
//                                                     }];
//
//                        break;
//                    case ComputerNameSerialForce:
//                        [imagrComponents addObject:@{@"type":@"computer_name",
//                                                     @"use_serial":@YES,
//                                                     @"auto":@YES
//                                                     }];
//                        break;
//
//                    default:
//                        break;
//                }
//
//
//
//            }
            NSURL *currWorkflowPackageURL=[[[resourcePrefix URLByAppendingPathComponent:@"Workflows"] URLByAppendingPathComponent:workflow.workflowName]  URLByAppendingPathComponent:@"Packages"];

            if (workflow.shouldEraseAndInstall==YES) {

                NSDictionary *eraseVolumeDict=@{@"type":@"script",
                                            @"content":@"#!/bin/bash\ndiskutil reformat \"{{target_volume}}\"",
                                            @"first_boot":@NO};


                [imagrComponents addObject:eraseVolumeDict];
                NSMutableDictionary *installOSDictionary=[@{@"type":@"startosinstall",
                                                                                                @"url":
                                                                                                    [[OSURL URLByAppendingPathComponent:[self.installMediaURL lastPathComponent]] description]} mutableCopy];

                if (workflow.usePackagesFolderPath==YES) {

                    NSArray *filePathsOfPackages=[self packagesPathsInFolder:workflow.packagesFolderPath];
                    NSMutableArray *packagesPaths=[NSMutableArray arrayWithCapacity:filePathsOfPackages.count];
                    [filePathsOfPackages enumerateObjectsUsingBlock:^(NSString *packageName, NSUInteger idx, BOOL * _Nonnull stop) {
                        [packagesPaths addObject:[[currWorkflowPackageURL URLByAppendingPathComponent:packageName] description]];
                    }];
                    if (packagesPaths.count>0) {

                        [packagesPaths addObject:[[currWorkflowPackageURL URLByAppendingPathComponent:@"TCSCleanup.pkg"] description]];

                        installOSDictionary[@"additional_package_urls"]=[NSArray arrayWithArray:packagesPaths];

                    }



                }
                [imagrComponents addObject:[NSDictionary dictionaryWithDictionary:installOSDictionary]];


            }


            if (workflow.usePackagesFolderPath && workflow.shouldEraseAndInstall==NO){
#warning make sure workflows have unique names or this all falls down.
                NSArray *filePathsOfPackages=[self packagesPathsInFolder:workflow.packagesFolderPath];
                [filePathsOfPackages enumerateObjectsUsingBlock:^(NSString *packagePath, NSUInteger idx, BOOL * _Nonnull stop) {
                                    [imagrComponents addObject:@{@"type":@"package",
                                                                 @"url":[[currWorkflowPackageURL URLByAppendingPathComponent:packagePath.lastPathComponent] description],
                                                                 @"first_boot":@NO}];

                }];


            }//usePackagesFolderPath

//            if (workflow.useScriptFolderPath){
//
//                NSError *err;
//                NSFileManager *fm=[NSFileManager defaultManager];
//                NSArray *filePathsOfScripts=[fm contentsOfDirectoryAtPath:workflow.scriptFolderPath error:&err];
//                if (filePathsOfScripts) {
//
//                    [filePathsOfScripts enumerateObjectsUsingBlock:^(NSString *scriptPath, NSUInteger idx, BOOL * _Nonnull stop) {
//
//
//                    [imagrComponents addObject:@{@"type":@"script",
//                                                 @"url":[[scriptsURL URLByAppendingPathComponent:scriptPath.lastPathComponent] description],
//                                                 @"first_boot":@NO}];
//                    }];
//                }
//                else {
//                    NSLog(@"package path does not exist: %@",err);
//                }
//
//            }//usePackagesFolderPath


//have to have *something* in components to include the workflow
            if (imagrComponents && imagrComponents.count>0) [imagrWorkflow setObject:imagrComponents forKey:@"components"];
        } //workflow is active

        //have to having something in components to add workflow to workflows.
        if (imagrWorkflow && [[imagrWorkflow objectForKey:@"components"] count]>0) [imagrWorkflows addObject:imagrWorkflow];

        [bookmarkedURL stopAccessingSecurityScopedResource];

    }];
    
    if (imagrWorkflows) [imagrDict setObject:imagrWorkflows forKey:@"workflows"];

    return imagrDict;

}
-(NSArray *)packagesPathsInFolder:(NSString *)folderPath{
    NSError *err;
    NSFileManager *fm=[NSFileManager defaultManager];
    NSMutableArray *arrayOfPaths=[NSMutableArray array];
    NSArray *directoryContents=[fm contentsOfDirectoryAtPath:folderPath error:&err];
    if (directoryContents && directoryContents.count>0) {

        [directoryContents enumerateObjectsUsingBlock:^(NSString *packagePath, NSUInteger idx, BOOL * _Nonnull stop) {

            if ([packagePath.pathExtension.lowercaseString isEqualToString:@"pkg"]) {
                [arrayOfPaths addObject:packagePath];
            }
            else {
                NSLog(@"non-package item found: %@",packagePath);
            }
        }];
    }
    else {
        NSLog(@"package path does not exist: %@",err);
    }
    return [[NSArray arrayWithArray:arrayOfPaths] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}
@end
