//
//  Workflow.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/23/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, RestartAction) {
    None,
    Restart,
    Shutdown


};

typedef NS_ENUM(NSUInteger, ComputerName) {
    ComputerNameAsk,
    ComputerNameSerialAsk,
    ComputerNameSerialForce
};
@interface TCSWorkflowLocalization : NSObject <NSCopying,NSSecureCoding>
@property (strong) NSString *keyboardLayout;
@property (strong) NSString *language;
@property (strong) NSString *locale;
@property (strong) NSString *timezone;
@end


@interface TCSWorkflow : NSObject <NSCopying,NSSecureCoding>
@property (assign) BOOL isActive;
@property (assign) BOOL isDefault;
@property (strong) NSString *workflowName;
@property (strong) NSString *workflowDescription;

@property (assign) BOOL useComputerName;
@property (assign) NSInteger computerName;

@property (assign) BOOL useScriptFolderPath;
@property (strong) NSString *scriptFolderPath;

@property (assign) BOOL usePackagesFolderPath;
@property (strong) NSString *packagesFolderPath;
@property (strong) NSData *packagesFolderBookmark;


@property (assign) BOOL shouldEraseAndInstall;
@property (assign) BOOL shouldBlessTarget;

@property (assign) BOOL useRestartAction;
@property (assign) RestartAction restartAction;

@property (assign) BOOL useWorkflowLocalization;

@property (strong) TCSWorkflowLocalization *workflowLocalization;
@end

NS_ASSUME_NONNULL_END
