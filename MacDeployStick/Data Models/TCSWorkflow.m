//
//  Workflow.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/23/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "TCSWorkflow.h"
@implementation TCSWorkflowLocalization

-(id)copyWithZone:(NSZone *)zone{
    TCSWorkflowLocalization *newWorkflowLocalization=[[TCSWorkflowLocalization alloc] init];
    newWorkflowLocalization.keyboardLayout=[_keyboardLayout copy];
    newWorkflowLocalization.language=[_language copy];
    newWorkflowLocalization.locale=[_locale copy];
    newWorkflowLocalization.timezone=[_timezone copy];
    return newWorkflowLocalization;
}


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.keyboardLayout = [decoder decodeObjectForKey:@"keyboardLayout"];
    self.language = [decoder decodeObjectForKey:@"language"];
    self.locale = [decoder decodeObjectForKey:@"locale"];
    self.timezone = [decoder decodeObjectForKey:@"timezone"];

    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {

    [encoder encodeObject:self.keyboardLayout forKey:@"keyboardLayout"];

    [encoder encodeObject:self.language forKey:@"language"];
    [encoder encodeObject:self.locale forKey:@"locale"];
    [encoder encodeObject:self.timezone forKey:@"timezone"];
}
+ (BOOL)supportsSecureCoding {
    return YES;
}
@end

@implementation TCSWorkflow
+ (BOOL)supportsSecureCoding {
    return YES;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.workflowLocalization=[[TCSWorkflowLocalization alloc] init];
        self.isActive=YES;

    }
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone {

    TCSWorkflow *newWorkflow=[[TCSWorkflow alloc] init];
    newWorkflow.workflowName=[_workflowName copy];
    newWorkflow.workflowDescription=[_workflowDescription copy];

    newWorkflow.useComputerName=_useComputerName ;
    newWorkflow.computerName=_computerName;

    newWorkflow.useScriptFolderPath=_useScriptFolderPath ;
    newWorkflow.scriptFolderPath=[_scriptFolderPath copy];

    newWorkflow.usePackagesFolderPath=_usePackagesFolderPath;
    newWorkflow.packagesFolderPath=[_packagesFolderPath copy];
    newWorkflow.packagesFolderBookmark=[_packagesFolderBookmark copy];

    newWorkflow.shouldEraseAndInstall=_shouldEraseAndInstall;
    newWorkflow.shouldBlessTarget=_shouldBlessTarget;

    newWorkflow.useRestartAction=_useRestartAction;
    newWorkflow.restartAction=_restartAction;
    newWorkflow.isActive=_isActive;
    newWorkflow.isDefault=NO;

    newWorkflow.useWorkflowLocalization=_useWorkflowLocalization;
    newWorkflow.workflowLocalization=[_workflowLocalization copy];

    return newWorkflow;
}
- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.isActive = [decoder decodeBoolForKey:@"isActive"];
    self.isDefault = [decoder decodeBoolForKey:@"isDefault"];

    self.workflowName = [decoder decodeObjectForKey:@"workflowName"];
    self.workflowDescription = [decoder decodeObjectForKey:@"workflowDescription"];

    self.useComputerName = [decoder decodeBoolForKey:@"useComputerName"];

    self.computerName = [decoder decodeIntegerForKey:@"computerName"];
    self.useScriptFolderPath = [decoder decodeBoolForKey:@"useScriptFolderPath"];
    self.scriptFolderPath = [decoder decodeObjectForKey:@"scriptFolderPath"];
    self.usePackagesFolderPath = [decoder decodeBoolForKey:@"usePackagesFolderPath"];
    self.packagesFolderPath = [decoder decodeObjectForKey:@"packagesFolderPath"];
    self.packagesFolderBookmark = [decoder decodeObjectForKey:@"packagesFolderBookmark"];

    self.shouldEraseAndInstall = [decoder decodeBoolForKey:@"shouldEraseAndInstall"];
    self.shouldBlessTarget = [decoder decodeBoolForKey:@"shouldBlessTarget"];
    self.useRestartAction = [decoder decodeBoolForKey:@"useRestartAction"];
    self.restartAction = [decoder decodeIntegerForKey:@"restartAction"];
    self.useWorkflowLocalization = [decoder decodeBoolForKey:@"useWorkflowLocalization"];
    self.workflowLocalization = [decoder decodeObjectForKey:@"workflowLocalization"];

//    @property (assign) RestartAction restartAction;


    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeBool:self.isActive forKey:@"isActive"];
    [encoder encodeBool:self.isDefault forKey:@"isDefault"];

    [encoder encodeObject:self.workflowName forKey:@"workflowName"];
    [encoder encodeObject:self.workflowDescription forKey:@"workflowDescription"];

    [encoder encodeBool:self.useComputerName forKey:@"useComputerName"];
    [encoder encodeInteger:self.computerName forKey:@"computerName"];
    [encoder encodeBool:self.useScriptFolderPath forKey:@"useScriptFolderPath"];
    [encoder encodeObject:self.scriptFolderPath forKey:@"scriptFolderPath"];
    [encoder encodeBool:self.usePackagesFolderPath forKey:@"usePackagesFolderPath"];
    [encoder encodeObject:self.packagesFolderPath forKey:@"packagesFolderPath"];
    [encoder encodeObject:self.packagesFolderBookmark forKey:@"packagesFolderBookmark"];


    [encoder encodeBool:self.shouldEraseAndInstall forKey:@"shouldEraseAndInstall"];
    [encoder encodeBool:self.shouldBlessTarget forKey:@"shouldBlessTarget"];
    [encoder encodeBool:self.useRestartAction forKey:@"useRestartAction"];
    [encoder encodeInteger:self.restartAction forKey:@"restartAction"];
    [encoder encodeBool:self.useWorkflowLocalization forKey:@"useWorkflowLocalization"];
    [encoder encodeObject:self.workflowLocalization forKey:@"workflowLocalization"];




}
@end
