//
//  DeploySettings.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSWorkflow.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeploySettings : NSObject <NSSecureCoding>

@property (nonatomic,strong) NSURL *backgroundImageURL;
@property (nonatomic,strong) NSURL *installMediaURL;
@property (nonatomic,strong) NSData *installMediaBookmark;
@property (nonatomic,strong) NSURL *externalVolumeURL;
@property (nonatomic,strong) NSMutableArray *workflows;
@property (nonatomic,assign) BOOL shouldSaveToDMG;
@property (strong)  NSString *saveFilePath;
@property (atomic,assign) BOOL stopCopying;

+ (id)sharedSettings ;
-(void)addWorkflow:(TCSWorkflow *)workflow;
-(void)replaceWorkflow:(TCSWorkflow *)oldWorkflow withWorkflow:(TCSWorkflow *)workflow;
-(void)saveToPrefs:(id)sender;
-(NSDictionary *)imagrDictionaryWithResourcePrefix:(NSURL  *)resourcePrefix;
-(BOOL)saveResourcesToURL:(NSURL *)inPath withPrefix:(nullable NSURL *)inPrefix error:(NSError **)error;
@end

NS_ASSUME_NONNULL_END
