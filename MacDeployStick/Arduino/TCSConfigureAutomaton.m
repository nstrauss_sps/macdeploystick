//
//  TCSConfigureAutomaton.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/31/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//
#include <sys/select.h>
#import "TCSConfigureAutomaton.h"
#include <stdio.h>
@interface TCSConfigureAutomaton ()
typedef NS_ENUM(NSUInteger, SERIALMODE) {
    INITIALFLUSH,
    COMMAND,

};

@property (strong) NSTimer *checkForArduinoTimer;
@property (assign) int filedesc;
@property (strong) NSTimer *serialInputTimer;
@property (strong) NSString *leftoverString;
@property (assign) SERIALMODE mode;
@property (strong) id serialNotification;
@property (strong) NSString *command;
@property (assign) NSInteger startupDelay;
@property (assign) NSInteger version;
@property (assign) BOOL dataLoaded;
@end

@implementation TCSConfigureAutomaton

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
-(void)viewDidAppear{

    self.dataLoaded=NO;
   self.serialNotification=[[NSNotificationCenter defaultCenter] addObserverForName:@"TCSSerialDataLine" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {

        switch (self.mode) {
            case INITIALFLUSH:{
                self.mode=COMMAND;

                ssize_t n = write(self.filedesc, "get_settings\r\n", 14);
                if (n < 0)
                    fputs("write() of 16 bytes failed!\n", stderr);
            }
                break;
            case COMMAND:

                [self processCommand:[note.userInfo objectForKey:@"line"]];

                break;
        }


    }];

    self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:0.5 repeats:YES block:^(NSTimer * _Nonnull timer) {
        NSFileManager *fm=[NSFileManager defaultManager];

        NSError *err;
        NSArray *devices=[fm contentsOfDirectoryAtPath:@"/dev" error:&err];

        if (!devices) {
            NSLog(@"no devices!");
            return;
        }
        __block int count=0;
        __block NSString *devicePath=nil;
        
        [devices enumerateObjectsUsingBlock:^(NSString *currDeviceName, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *currDevicePath=nil;

            currDevicePath=[@"/dev" stringByAppendingPathComponent:currDeviceName];

            if ([currDevicePath containsString:@"cu.usbmodem"]) {
                devicePath=currDevicePath;
                NSLog(@"found %@",devicePath);
                count++;
            }

        }];
        if (count>1) {
            NSAlert *alert=[[NSAlert alloc] init];
//            alert.mess
            alert.messageText=@"more than 1 device found. app can only configure one device at a time";
            [alert runModal];
            [self dismissController:self];

            return;
        }
        if (count==1) {
            NSLog(@"device found. trying to connect");
            [self.checkForArduinoTimer invalidate];
            self.checkForArduinoTimer=nil;
            [self connect:devicePath];
        }
        

    }];


}
-(void)processCommand:(NSString *)inCommand{

    NSMutableString *workingString=[inCommand mutableCopy];
    if ([inCommand hasPrefix:@">"]){
        [workingString deleteCharactersInRange:NSMakeRange(0, 1)];
    }

    if ([workingString hasPrefix:@"{"] && [workingString hasSuffix:@"}"]) {

        //we have JSON
        NSError *error;
        NSDictionary *settingsDict = [NSJSONSerialization
                         JSONObjectWithData:[workingString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];

        [self.serialInputTimer invalidate];
        self.serialInputTimer=nil;

        self.startupDelay=[settingsDict[@"startup_delay"] integerValue];
        self.command=settingsDict[@"command"];
        self.version=[settingsDict[@"version"] integerValue];
        self.dataLoaded=YES;
    }
    else NSLog(@"invalid JSON");
}
-(void)viewWillDisappear{
    [self.checkForArduinoTimer invalidate];
    [self.serialInputTimer invalidate];
    [[NSNotificationCenter defaultCenter] removeObserver:self.serialNotification];
}
-(void)connect:(NSString *)inDevicePath{

    self.filedesc = open( inDevicePath.UTF8String, O_RDWR );

    if (self.filedesc<0) {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Connection Error";
        alert.informativeText=@"The connection could not be opened to the arduino. Please make sure no other processes are connected.";
        [alert runModal];
        [self dismissController:self];
        return;

    }
    self.mode=INITIALFLUSH;

    ssize_t n = write(self.filedesc, "\r\nget_settings\r\n", 16);
    if (n < 0)
        fputs("write() of 16 bytes failed!\n", stderr);

    self.serialInputTimer=[NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [self checkForSerialInput];
    }];




    
}

-(void)checkForSerialInput{
    int rv;
    char buff[255];
    int len = 255;
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 10000;
    fd_set set;

    FD_ZERO(&set); /* clear the set */
    FD_SET(self.filedesc, &set); /* add our file descriptor to the set */

    rv = select(self.filedesc + 1, &set, NULL, NULL, &timeout);
    if(rv == -1)
        perror("select"); /* an error accured */
    else if(rv == 0){
        //timeout
    }
    else {
        ssize_t total_bytes=read( self.filedesc, buff, len ); /* there was data to read */
        NSString *readString=[[NSString alloc] initWithBytes:buff length:total_bytes encoding:NSUTF8StringEncoding];
        if (self.leftoverString){
            readString=[self.leftoverString stringByAppendingString:readString];
            self.leftoverString=nil;
        }
        if ([readString containsString:@"\r\n"]){
            NSArray *lines=[readString componentsSeparatedByString:@"\r\n"];

            self.leftoverString=[lines lastObject];

            [lines enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

                [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSSerialDataLine" object:self userInfo:@{@"line":obj}];
                if (idx==lines.count-2) *stop=YES;

            }];
        }

    }

}
- (IBAction)updateArduino:(id)sender {
    if (!self.command){
        NSBeep();
        return;

    }
    NSDictionary *dict=@{@"command":self.command,@"startup_delay":@(self.startupDelay)};

    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0 error:&error];


    NSString *jsonString=[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];


    NSString *fullString=[NSString stringWithFormat:@"set_settings %@\r\n",jsonString];
    ssize_t n = write(self.filedesc, fullString.UTF8String,fullString.length);
    if (n < 0) {

        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Data write error";
        alert.informativeText=@"The data could not be written. Please check the arduino and try again.";
        [alert runModal];
        return;

    }
    else {
        NSAlert *alert=[[NSAlert alloc] init];
        alert.messageText=@"Update Successful";
        alert.informativeText=@"The arduino was updated with the new values. Please unplug now.";
        [alert runModal];


    }
    [self dismissController:self];

}

@end
