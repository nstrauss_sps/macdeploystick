//
//  TCSFlashArduino.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/30/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "TCSFlashArduino.h"

@interface TCSFlashArduino ()
@property (strong) NSTimer *checkForArduinoTimer;
@property (atomic,assign) BOOL waitingToProgram;
@property (atomic,assign) BOOL isProgramming;
@property (strong) NSArray *foundDevices;
@property (strong) NSString *selectedDevice;

@end

@implementation TCSFlashArduino

-(void)viewWillDisappear{
    [self.checkForArduinoTimer invalidate];
}
- (void)viewDidLoad {
    [super viewDidLoad];


}
-(void)checkForNewDevices:(id)sender{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{


        NSBundle *mainBundle=[NSBundle mainBundle];
        NSString *avrdudeFolder=[mainBundle pathForResource:@"avrdude" ofType:@""];
        NSString *avrDude=[[[[mainBundle bundlePath] stringByAppendingPathComponent:@"Contents"] stringByAppendingPathComponent:@"macOS"] stringByAppendingPathComponent:@"avrdude"];
        NSString *avrDudeConf=[avrdudeFolder stringByAppendingPathComponent:@"avrdude.conf"];

        NSFileManager *fm=[NSFileManager defaultManager];
        NSError *err;
        NSArray *devices=[fm contentsOfDirectoryAtPath:@"/dev" error:&err];

        if (!devices) {
            NSLog(@"no devices!");
            return;
        }
        __block NSString *devicePath=nil;
        [devices enumerateObjectsUsingBlock:^(NSString *currDeviceName, NSUInteger idx, BOOL * _Nonnull stop) {

            devicePath=[@"/dev" stringByAppendingPathComponent:currDeviceName];

            if ([devicePath containsString:@"cu.usbmodem"] && ![self.foundDevices containsObject:@{@"devicePath":devicePath}]){

                NSLog(@"found %@",devicePath);
                NSTask *task=[[NSTask alloc] init];
                task.launchPath=avrDude;
                task.arguments=@[@"-p",@"atmega32u4",@"-c",@"avr109",@"-P",devicePath,@"-C",avrDudeConf,@"-q",@"-q"];

                [task launch];
                [task waitUntilExit];
                if (task.terminationStatus==0){
                    self.foundDevices=[self.foundDevices arrayByAddingObject:@{@"devicePath":devicePath}];
                    if (self.foundDevices.count==1) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                        self.selectedDevice=devicePath;
                        });

                    }

                }
                else {
                    devicePath=nil;
                }
            }
            else {
                devicePath=nil;
            }
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"scheduling new timer");
            self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForNewDevices:) userInfo:nil repeats:NO];

        });
    });
}
    
-(void)viewWillAppear{
    self.foundDevices=@[];
    self.checkForArduinoTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkForNewDevices:) userInfo:nil repeats:NO];



}
-(IBAction)programDevice:(id)sender{
    [self.checkForArduinoTimer invalidate];
    NSFileManager *fm=[NSFileManager defaultManager];

    NSString *inDevicePath=self.selectedDevice;

    self.waitingToProgram=YES;

//programmingsheet
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        int i;
        for (i=0;i<10;i++ ){
            if ([fm fileExistsAtPath:inDevicePath]) {
                break;
            }
            sleep(1);
        }


        if ([fm fileExistsAtPath:inDevicePath]==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.isProgramming=NO;
                self.waitingToProgram=NO;
                NSAlert *alert=[[NSAlert alloc] init];
                alert.messageText=@"Arduino Not Found";
                alert.informativeText=@"The Arduino automaton could not found! Please try again later.";
                [alert runModal];
                self.foundDevices=@[];
                self.selectedDevice=nil;
                [self checkForNewDevices:self];
            });

        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                //            self.waitingToProgram=NO;
                self.isProgramming=YES;
            });
            NSBundle *mainBundle=[NSBundle mainBundle];
            NSString *avrdudeFolder=[mainBundle pathForResource:@"avrdude" ofType:@""];
            NSString *avrDude=[[[[mainBundle bundlePath] stringByAppendingPathComponent:@"Contents"] stringByAppendingPathComponent:@"macOS"] stringByAppendingPathComponent:@"avrdude"];
            NSString *avrDudeConf=[avrdudeFolder stringByAppendingPathComponent:@"avrdude.conf"];


            NSString *firmwarePath=[mainBundle pathForResource:@"arduino_firmware" ofType:@"hex"];
            NSTask *task=[[NSTask alloc] init];
            task.launchPath=avrDude;
            NSString *operation=[NSString stringWithFormat:@"flash:w:%@",firmwarePath];
            task.arguments=@[@"-p",@"atmega32u4",@"-U",operation,@"-c",@"avr109",@"-P",inDevicePath,@"-C",avrDudeConf,@"-q",@"-q"];

            [task launch];
            [task waitUntilExit];
            dispatch_async(dispatch_get_main_queue(), ^{

                self.isProgramming=NO;
                self.waitingToProgram=NO;

                if (task.terminationStatus!=0){
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Programming Failed!";
                    alert.informativeText=@"Programming failed. Please check the arduino and try again.";
                    [alert runModal];

                }
                else {

                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Flash successful";
                    alert.informativeText=@"The Arduino automaton was successfully flashed. Please disconnect from the USB port now.";
                    [alert runModal];

                }

                [self dismissController:self];
            });

        }
    });


}

@end
