//
//  TCSUtility.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSUtility : NSObject
+(NSArray *)listOfMedia;
@end

NS_ASSUME_NONNULL_END
