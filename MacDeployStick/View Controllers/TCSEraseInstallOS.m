//
//  TCSEraseInstallOS.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSEraseInstallOS.h"
#import "TCSUtility.h"
@interface TCSEraseInstallOS ()
@property (strong) NSString *macOSInstallerPath;
@property (strong) NSImage *macOSInstallerImage;
@property (strong) NSString *targetVolumePath;
@property (strong) NSImage *targetVolumeImage;

@property (strong) NSString *commandString;
@property (nonatomic,strong) NSArray *listOfMedia;

@end

@implementation TCSEraseInstallOS
- (IBAction)targetChanged:(id)sender {

    if (self.targetVolumePath) {
        self.targetVolumeImage=[[NSWorkspace sharedWorkspace] iconForFile:self.targetVolumePath];


    }

    [self updateCommand];

}
-(void)updateCommand{
    if (self.macOSInstallerPath && self.targetVolumePath) {
        NSString *createInstallMediaURL=[self.macOSInstallerPath stringByAppendingPathComponent:@"Contents/Resources/createinstallmedia"];

        self.commandString=[NSString stringWithFormat:@"sudo \"%@\" --volume \"%@\"",createInstallMediaURL,self.targetVolumePath];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
-(void)viewWillAppear{

    self.listOfMedia =[TCSUtility listOfMedia];


}

-(IBAction)selectMacOSInstaller:(id)sender{
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowedFileTypes=@[@"app"];
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a macOS installer app";
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        NSURL *selectedFileURL=[openPanel URL];
        NSURL *createInstallMediaURL=[selectedFileURL URLByAppendingPathComponent:@"Contents/Resources/createinstallmedia"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:createInstallMediaURL.path]==YES){

            self.macOSInstallerPath=[openPanel URL].path;
            self.macOSInstallerImage=[[NSWorkspace sharedWorkspace] iconForFile:self.macOSInstallerPath];
            [self updateCommand];

        }
        else {
            NSAlert *alert=[[NSAlert alloc] init];
            alert.messageText=@"Invalid Installer";

            alert.informativeText=@"The installer did not contain createinstallmedia. Please select a valid macOS installer";


            [alert runModal];
            return;

        }

    }

}



//-(void)ethod{
//
//
//
//            NSAlert *alert=[[NSAlert alloc] init];
//            alert.messageText=@"Install macOS on External Volume";
//            NSString *runString=[NSString stringWithFormat:@"Please copy and paste the command below in Terminal to install macOS on the external drive. Click Command Completed to continue when the command completes successfully in Terminal.\n\nsudo \"%@\" --volume \"%@\"",[createInstallMediaURL path],volumePath];
//            alert.informativeText=runString;
//            [alert addButtonWithTitle:@"Command Completed"];
//            [alert addButtonWithTitle:@"Cancel"];
//            NSInteger res=  [alert runModal];
//
//            if (res==NSAlertSecondButtonReturn) return;
//
//
//        }
//
//    }
//
//
//}
@end
