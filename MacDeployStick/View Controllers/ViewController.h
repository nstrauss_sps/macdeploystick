//
//  ViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "DeploySettings.h"
@interface ViewController : NSViewController  <NSTextViewDelegate>
//@property (nonatomic, strong) DeploySettings *deploySettings;
-(DeploySettings *)deploySettings;

-(IBAction)checkForResources:(id)sender;
@end

