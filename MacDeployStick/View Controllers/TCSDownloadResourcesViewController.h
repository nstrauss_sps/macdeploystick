//
//  TCSDownloadResourcesViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSDownloadResourcesViewController : NSViewController

@end

NS_ASSUME_NONNULL_END
