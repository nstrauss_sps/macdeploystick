//
//  ExportMediaViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSCopyFileController.h"
NS_ASSUME_NONNULL_BEGIN

@interface ExportMediaViewController : NSViewController <TCSFileCopyDelegateProtocol>


@end

NS_ASSUME_NONNULL_END
