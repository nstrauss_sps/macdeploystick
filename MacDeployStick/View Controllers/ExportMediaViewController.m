//
//  ExportMediaViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "ExportMediaViewController.h"
#import "DeploySettings.h"
#import "TCSUtility.h"
#import "TCTaskWrapperWithBlocks.h"

@interface ExportMediaViewController ()

@property (nonatomic,strong) NSArray *listOfMedia;
@property (assign) BOOL isRunning;
@property (assign) BOOL isInderminateProgress;
@property (assign) BOOL isCancelling;
@property (assign) BOOL savingToDMG;
@property (assign) float percentComplete;
@property (strong) TCSCopyFileController *fileCopyController;
@property (strong) NSString *tempFolderPath;
@property (strong) NSString *status;
//@property (strong) (id)notificationRef;
@property (nonatomic, strong) NSString *resourceFolderURLPrefix;
@property (strong) TCTaskWrapperWithBlocks *createDiskImageTaskWrapper;
@property (strong) TCTaskWrapperWithBlocks *convertWrapper;

@property (strong)NSURL *installMediaBookmarkResolved;

@end

@implementation ExportMediaViewController

-(void)viewWillAppear{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showErrorMessage:) name:@"TCSCopyFailed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateStatusItem:) name:@"TCSCopyItemStatus" object:nil];

    self.listOfMedia =[TCSUtility listOfMedia];
    if ([[DeploySettings sharedSettings]
          shouldSaveToDMG]==NO) {


    [self saveResourcesToPath:[[DeploySettings sharedSettings] externalVolumeURL].path prefix:nil];
    }
    else {


        self.tempFolderPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSUUID UUID] UUIDString]];
        self.isRunning=YES;

        NSError *err;
        if ([[NSFileManager defaultManager] createDirectoryAtPath:self.tempFolderPath withIntermediateDirectories:NO attributes:nil error:&err]==NO) {
            self.isRunning=NO;

            [[NSAlert alertWithError:err] runModal];
            return;
        }

        self.savingToDMG=YES;

        [self saveResourcesToPath:self.tempFolderPath prefix:[NSURL fileURLWithPath:@"/Volumes/MacDeployStick"]];


    }



}
-(void)viewWillDisappear{
    [[NSNotificationCenter defaultCenter] removeObserver:self];



}
-(void)updateStatusItem:(NSNotification *)not{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.percentComplete+=5;

        self.status=not.userInfo[@"message"];
    });


}
-(void)showErrorMessage:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.isRunning=NO;
        [[NSAlert alertWithError:notification.userInfo[@"error"]] runModal];
        [self dismissController:self];
    });

}
-(void)saveResourcesToPath:(NSString *)inPath prefix:(NSURL *)inPrefix{
    self.isRunning=YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        BOOL stop=NO;
        NSError *error;
        if([[DeploySettings sharedSettings] saveResourcesToURL:[NSURL fileURLWithPath:inPath] withPrefix:inPrefix error:&error]==NO && self.isCancelling==NO) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:self userInfo:@{@"error":error}];
            stop=YES;
        }


        if (stop==NO && self.isCancelling==NO) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self copyOS];
            });

        }
        if (self.isCancelling==YES) {
            dispatch_async(dispatch_get_main_queue(), ^{

            self.isRunning=NO;
            [self dismissController:self];
            });

        }
    });


}
-(void)copyOS{
    NSFileManager *fm=[NSFileManager defaultManager];
   NSString *externalVolume= [[[DeploySettings sharedSettings] externalVolumeURL] path];
    NSString *osDestinationPath;
    if (self.savingToDMG==NO) {
    osDestinationPath=[[externalVolume stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"macOS"];
    }
    else {
        osDestinationPath=[[self.tempFolderPath stringByAppendingPathComponent:@"Deploy"] stringByAppendingPathComponent:@"macOS"];


    }
    NSError *error;
    NSString *sourceOSFile=[[[DeploySettings sharedSettings] installMediaURL] path];
    self.status=[NSString stringWithFormat:@"Copying %@",sourceOSFile.lastPathComponent];
    NSString *osDMGDestination=[osDestinationPath stringByAppendingPathComponent:sourceOSFile.lastPathComponent];

    if (![fm fileExistsAtPath:osDestinationPath]){
        if([fm createDirectoryAtPath:osDestinationPath withIntermediateDirectories:YES attributes:nil error:&error]==NO ){
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"TCSCopyFailed" object:error];
        }
    }

    if ([fm fileExistsAtPath:osDMGDestination]==NO){
        self.fileCopyController=[[TCSCopyFileController alloc] init];

        if ([[DeploySettings sharedSettings] installMediaBookmark]) {

        }
        [self.fileCopyController copyFile:sourceOSFile toPath:osDestinationPath delegate:self];
    }
    else {

        [self copyCompleted:self withError:nil];
    }


}
-(void)percentCompleted:(float)percentCompleted{

    if (percentCompleted>self.percentComplete) {
        self.percentComplete=percentCompleted;
    }
}
-(void)copyCompleted:(id)sender withError:(NSError *)error{
    if (self.savingToDMG==YES) {
        NSFileManager *fm=[NSFileManager defaultManager];

        NSString *saveFilename=[[[DeploySettings sharedSettings] saveFilePath] lastPathComponent];

        NSString *tempSaveFilePath=[NSTemporaryDirectory() stringByAppendingPathComponent:saveFilename];
        if ([fm fileExistsAtPath:tempSaveFilePath]) {
            NSError *err;
            if ([fm removeItemAtPath:tempSaveFilePath error:&err]==NO) {
                [[NSAlert alertWithError: err] runModal];
                [self dismissController:self];
                return;
            }
        }
        NSString *tempSaveFilePathConverted=[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_converted.dmg",saveFilename]];
        if ([fm fileExistsAtPath:tempSaveFilePathConverted]) {
            NSError *err;
            if ([fm removeItemAtPath:tempSaveFilePathConverted error:&err]==NO) {
                [[NSAlert alertWithError: err] runModal];
                [self dismissController:self];
                return;
            }
        }
        self.percentComplete=self.percentComplete+5;
        self.status=@"Saving to Disk Image";
        self.createDiskImageTaskWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{


        } endBlock:^{

            if (self.isCancelling==NO && self.createDiskImageTaskWrapper.terminationStatus==0) {
                self.convertWrapper=[[TCTaskWrapperWithBlocks alloc] initWithStartBlock:^{
                    self.status=@"Converting Disk Image";
                    self.percentComplete=self.percentComplete+25;

                } endBlock:^{
                    NSError *err;
                    sleep(1);
                    if([fm moveItemAtPath:tempSaveFilePathConverted toPath:[[DeploySettings sharedSettings] saveFilePath] error:&err]==NO){

                        [[NSAlert alertWithError:err] runModal];
                    }

                    if ([[NSFileManager defaultManager] removeItemAtPath:tempSaveFilePath error:&err]==NO) {


                        [[NSAlert alertWithError:err] runModal];
                        [self dismissController:self];
                        return;
                    }

                    if ([[NSFileManager defaultManager] removeItemAtPath:self.tempFolderPath error:&err]==NO) {


                        [[NSAlert alertWithError:err] runModal];
                        [self dismissController:self];
                        return;


                    }
                    if (self.isCancelling==NO) {
                        self.savingToDMG=NO;
                        self.isRunning=NO;

                        NSAlert *alert=[[NSAlert alloc] init];
                        alert.messageText=@"Disk Image Saved Successfully";
                        alert.informativeText=@"The disk image was saved successfully.";
                        [alert runModal];
                        [self dismissController:self];
                    }
                } outputBlock:^(NSString *output) {
                    NSLog(@"%@",output);
                } errorOutputBlock:^(NSString *errorOutput) {

                    NSLog(@"%@",errorOutput);
                } arguments:@[@"/usr/bin/hdiutil",@"convert",@"-format",@"UDRO",@"-ov",@"-o",tempSaveFilePathConverted,tempSaveFilePath]];
                [self.convertWrapper startProcess];
            }
            else{
                self.savingToDMG=NO;
                self.isRunning=NO;
            }


        } outputBlock:^(NSString *output) {
            if([output containsString:@"PERCENT:"]) {


                NSString *percentString=[[output componentsSeparatedByString:@"PERCENT:"] lastObject];

                if ([percentString floatValue]>0) self.percentComplete=[percentString floatValue];
            }
            NSLog(@"%@",output);
        } errorOutputBlock:^(NSString *errorOutput) {


            NSLog(@"%@",errorOutput);

        } arguments:@[@"/usr/bin/hdiutil",@"makehybrid",@"-hfs",@"-default-volume-name",@"MacDeployStick",@"-o",tempSaveFilePath,self.tempFolderPath]];


        /*
         } arguments:@[@"/usr/bin/hdiutil",@"makehybrid",@"-hfs",@"-default-volume-name",@"MacDeployStick",@"-o",[[DeploySettings sharedSettings] saveFilePath],self.tempFolderPath]];


         hdiutil makehybrid ${HDIUTIL_VERBOSITY} -default-volume-name "${VOLUME_NAME}" -hfs -o "${DMG_TEMP_NAME}" "$SRC_FOLDER"
         hdiutil convert -format UDRW -ov -o "${DMG_TEMP_NAME}" "${DMG_TEMP_NAME}"
*/

//        } arguments:@[@"/usr/bin/hdiutil",@"create",@"-puppetstrings",@"-fs",@"JHFS+",@"-volname",@"MacDeployStick",@"-srcfolder",self.tempFolderPath,[[DeploySettings sharedSettings] saveFilePath]]];

        [self.createDiskImageTaskWrapper startProcess];
        return;
    }
    else {
        self.isRunning=NO;
        if (self.isCancelling==NO && error) {
            [[NSAlert alertWithError:error] runModal];
            [self dismissController:self];
            return;

        }
        if (self.isCancelling==NO && error==nil) {
            [self showSuccess];
        }
        self.isCancelling=NO;
    }




}
-(void)showSuccess{
    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Resources Saved Successfully";
    alert.informativeText=@"The resources were exported successfully.";
    [alert runModal];

    [self dismissController:self];

}
//- (IBAction)actionButtonPressed:(id)sender {
//
//
//
//    if (self.shouldSaveToExternalVolume==YES) {
//        self.isRunning=YES;
//        [self saveResourcesToPath:[[DeploySettings sharedSettings] externalVolumeURL].path prefix:nil];
//
//
//
//    }
//    else if (self.shouldSaveToDMG==YES) {
//
//
//
//
//
//    }



//
//        // NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:Path error:&attributesError];
//
//        NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
//
//        [[NSFileManager defaultManager] attributesOfItemAtPath:<#(nonnull NSString *)#> error:<#(NSError * _Nullable __autoreleasing * _Nullable)#>]
//
//        NSString *saveFilePath=[savePanel URL].path;
//
//

//}
- (IBAction)stopRunning:(id)sender {
    self.isCancelling=YES;
    if (self.isRunning==YES){

        if (self.createDiskImageTaskWrapper) [self.createDiskImageTaskWrapper stopProcess];
        if (self.fileCopyController) [self.fileCopyController cancelCopyOperation];
        if (self.convertWrapper) [self.convertWrapper stopProcess];
        [[DeploySettings sharedSettings] setStopCopying:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.shouldSaveToExternalVolume=YES;
//    self.shouldSaveToDMG=NO;
//
//    self.shouldSaveToExternalVolume=YES;
    // Do view setup here.
}

@end
