//
//  WorkflowViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "WorkflowViewController.h"
#import "DeploySettings.h"

@interface WorkflowViewController ()
@end

@implementation WorkflowViewController
- (IBAction)selectPackageFolder:(id)sender {
//
    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=NO;
    openPanel.canChooseDirectories=YES;
    openPanel.allowsMultipleSelection=NO;
    openPanel.message=@"Please select a packages folder:";
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {
        NSError *err;
        self.workflow.packagesFolderPath=[openPanel URL].path;
        self.workflow.usePackagesFolderPath=YES;
        NSData *selectedFileURLBookmark=[[openPanel URL] bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                                  includingResourceValuesForKeys:nil relativeToURL:nil error:&err];

        self.workflow.packagesFolderBookmark=selectedFileURLBookmark;
    }

}
- (void)controlTextDidChange:(NSNotification *)aNotification{


    NSInteger tag=[aNotification.object tag]+1;
    NSTextField *currentTextField=aNotification.object;
    NSInteger length=currentTextField.stringValue.length;
    NSButton *button=[self.view viewWithTag:tag];
    if (button ) {
        if (length>0) {
           if (button.state==NSControlStateValueOff) [button performClick:self];
        }
        else {
            if (button.state==NSControlStateValueOn) [button performClick:self];
        }

    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
-(void)awakeFromNib{
    if (!self.representedObject) {
        self.representedObject=[DeploySettings sharedSettings];
    }

}
- (IBAction)okButtonPressed:(id)sender {

    if (self.priorWorkflow!=nil) {
            [self.representedObject replaceWorkflow:self.priorWorkflow withWorkflow:self.workflow];

    }
    else {
        [self.representedObject addWorkflow:self.workflow];
    }

    [self dismissController:self];

}

@end
