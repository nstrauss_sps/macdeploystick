//
//  WorkflowViewController.h
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TCSWorkflow.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSUInteger, MODE) {
    modeExisting,
    modeNew
};
@interface WorkflowViewController : NSViewController
@property (nonatomic, strong) TCSWorkflow *workflow;
@property  (nonatomic, strong) TCSWorkflow * _Nullable  priorWorkflow;

@property (assign) MODE mode;
@end

NS_ASSUME_NONNULL_END
