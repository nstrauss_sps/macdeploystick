//
//  ViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 12/22/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"
#import "DeploySettings.h"
#import "WorkflowViewController.h"
#include <sys/mount.h>
#ifdef APPSTORE
#import <TCSStore/TCSStore.h>
#endif

#define kNotificationPurchaseComplete @"TCSPurchaseComplete"
#define kProductID @"100"
#define TRIALDAYS 0

typedef NS_ENUM(NSUInteger, TCSSAVEOPERATION) {
    SAVETOVOLUME,
    SAVETODMG,
};
@interface ViewController()
@property (nonatomic, strong) DeploySettings *deploySettings;
@property (nonnull,strong) NSURL *installMediaURL;

@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *arrayController;
@property (nonatomic,strong) NSMutableArray *workflows;
@property (nonatomic,strong) NSImage *installerImage;

@end

@implementation ViewController
- (IBAction)saveResourcesButtonPressed:(id)sender {
    switch ([sender tag]) {
        case SAVETOVOLUME:
        {
            BOOL resourcesNeeded=[self checkIfResourcesNeeded];

            if (resourcesNeeded==YES) {

                [self downloadResources];
            }
            else {

                NSOpenPanel *openPanel=[NSOpenPanel openPanel];

                openPanel.canChooseFiles=NO;
                openPanel.canChooseDirectories=YES;
                openPanel.allowsMultipleSelection=NO;

                openPanel.message=@"Select a volume to save resources:";
                NSModalResponse res=[openPanel runModal];

                if (res!=NSModalResponseOK) {

                    return;
                }

                NSString *openVolumePath=[openPanel URL].path;
                struct statfs output;
                statfs([openVolumePath UTF8String], &output);
                NSString *filesystemType=[NSString stringWithUTF8String:output.f_fstypename];
                NSLog(@"%@",filesystemType);
                if ([[filesystemType lowercaseString] containsString:@"hfs"]==NO) { //skip root
                    NSAlert *alert=[[NSAlert alloc] init];
                    alert.messageText=@"Volume is not HFS+";
                    alert.informativeText=@"The selected volume is not HFS+. This can cause issues. Are you sure you want to continue?";

                    [alert addButtonWithTitle:@"Cancel"];
                    [alert addButtonWithTitle:@"Continue"];
                    NSInteger res=[alert runModal];

                    if (res==NSAlertFirstButtonReturn) return;

                }

                [[DeploySettings sharedSettings] setExternalVolumeURL:[NSURL fileURLWithPath:openVolumePath]];
                [[DeploySettings sharedSettings] setShouldSaveToDMG:NO];

                [self performSegueWithIdentifier:@"TCSExportMediaSegue" sender:self];
            }
        }
            break;
        case SAVETODMG:
        {
            NSSavePanel *savePanel=[NSSavePanel savePanel];
            savePanel.canCreateDirectories=YES;
            savePanel.allowedFileTypes=@[@"dmg"];

            savePanel.message=@"Specify a name and location for the Disk Image:";
            NSModalResponse res=[savePanel runModal];

            if (res!=NSModalResponseOK) {

                return;
            }
            [[DeploySettings sharedSettings] setSaveFilePath:[savePanel URL].path];

            [[DeploySettings sharedSettings] setShouldSaveToDMG:YES];

            [self performSegueWithIdentifier:@"TCSExportMediaSegue" sender:self];



        }

            break;
        default:
            break;
    }
}
- (IBAction)tableDoubleClick:(id)sender {
    if ([self.tableView clickedRow]!=-1) {

        [self performSegueWithIdentifier:@"editWorkflow" sender:self];
    }
}
- (IBAction)backgroundImageButtonPressed:(id)sender {
    self.deploySettings=[DeploySettings sharedSettings];

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {


        [[DeploySettings sharedSettings] setBackgroundImageURL:[openPanel URL]];

    }

}
-(void)awakeFromNib{

    NSString *path=[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist" inDirectory:nil];

    NSDictionary *defaultDict=[NSDictionary dictionaryWithContentsOfFile:path];
    NSUserDefaults *ud=[NSUserDefaults standardUserDefaults];

    [ud registerDefaults:defaultDict];

    if (!self.representedObject) {
        self.representedObject=[DeploySettings sharedSettings];
    }
    if ([[DeploySettings sharedSettings] installMediaURL].path) self.installerImage=[[NSWorkspace sharedWorkspace] iconForFile:[[DeploySettings sharedSettings] installMediaURL].path];

    [self.tableView setDoubleAction:@selector(tableDoubleClick:)];
}
-(IBAction)checkForResources:(id)sender{


    BOOL resourcesNeeded=[self checkIfResourcesNeeded];

    if (resourcesNeeded==YES) {

        [self downloadResources];
    }

}

-(BOOL)checkIfResourcesNeeded{
    NSFileManager *fm=[NSFileManager defaultManager];

    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    NSString *imagrPath=[appSupportDir stringByAppendingPathComponent:@"Imagr.app"];

    if ([fm fileExistsAtPath:imagrPath]) return NO;
    return YES;

}
-(void)downloadResources{

    NSAlert *alert=[[NSAlert alloc] init];
    alert.messageText=@"Resource Needed";
    alert.informativeText=@"In order to function properly, resources need to be downloaded. Download now?";

    [alert addButtonWithTitle:@"OK"];
    [alert addButtonWithTitle:@"Cancel"];
    NSInteger res=[alert runModal];

    if (res==NSAlertSecondButtonReturn) return;




    [self performSegueWithIdentifier:@"TCSDownloadResourcesSegue" sender:self];
//

}
- (void)viewDidLoad {
    [super viewDidLoad];


}
#ifdef APPSTORE

- (IBAction)buyButtonPressed:(id)sender {
    [self buy:self];
}

-(void)buy:(id)sender{

    [[TCSStoreManager sharedManager] setMainWindow:[NSApp mainWindow]];


    [[TCSStoreManager sharedManager] setTrialDays:TRIALDAYS];
    //    [[TCSStoreManager sharedManager] setMoreInfoLink:@"https://twocanoes.com"];
    [[TCSStoreManager sharedManager] showTrialBlocker:self];


}
#endif
-(void)viewDidAppear{

#ifdef APPSTORE

    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationPurchaseComplete object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"Purchase completed");
    }];
    [[TCSStoreManager sharedManager] setMoreInfoLink:@"https://twocanoes.com/mac/in-app/mds-purchasing-help"];
    [[TCSStoreManager sharedManager] setTrialDays:14];

    [[TCSStoreManager sharedManager] startWithProductIdentifer:@"MDS100"];
    if ([[TCSStoreManager sharedManager] hasPurchased]==NO) {
        [self buy:self];
    }
    //    [[TCSStoreManager sharedManager] setNewsletterSignupLaunches:1];
    [[TCSStoreManager sharedManager] setNewsletterURL:@"https://twocanoes.com/subscribe/"];
    [[TCSStoreManager sharedManager] showNewsletterIfNeeded];

#endif


}
- (IBAction)selectInstallerButtonPressed:(id)sender {
    self.deploySettings=[DeploySettings sharedSettings];

    NSOpenPanel *openPanel=[NSOpenPanel openPanel];
    openPanel.canChooseFiles=YES;
    openPanel.canChooseDirectories=NO;
    openPanel.allowsMultipleSelection=NO;
    openPanel.allowedFileTypes=@[@"dmg"];
    NSModalResponse res=[openPanel runModal];

    if (res==NSModalResponseOK) {


        NSError *err;
        NSURL *selectedFileURL=[openPanel URL];
        NSData *selectedFileURLBookmark=[selectedFileURL bookmarkDataWithOptions:NSURLBookmarkCreationWithSecurityScope
                                                  includingResourceValuesForKeys:nil relativeToURL:nil error:&err];

        [[DeploySettings sharedSettings] setInstallMediaURL:selectedFileURL];
        [[DeploySettings sharedSettings] setInstallMediaBookmark:selectedFileURLBookmark];

        self.installerImage=[[NSWorkspace sharedWorkspace] iconForFile:[openPanel URL].path];
//        self.installerImage=[[]]

    }

}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (void)prepareForSegue:(NSStoryboardSegue *)segue sender:(NSButton *)sender{
    WorkflowViewController *workflowViewController=segue.destinationController;

    if ([segue.identifier isEqualToString:@"newWorkflow"]) {

        workflowViewController.workflow=[[TCSWorkflow alloc] init];
        workflowViewController.priorWorkflow=nil;
    }
    else if ([segue.identifier isEqualToString:@"editWorkflow"]) {

        workflowViewController.workflow=[[[self.arrayController selectedObjects] firstObject] copy];
        workflowViewController.priorWorkflow=[[self.arrayController selectedObjects] firstObject];

    }

}

@end
