//
//  TCSCopyFileController.h
//  Winclone
//
//  Created by Tim Perfitt on 5/5/18.
//  Copyright © 2018 Twocanoes Software Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol TCSFileCopyDelegateProtocol
-(void)percentCompleted:(float)percentCompleted;
-(void)copyCompleted:(id)sender withError:(NSError *)error;


@end

@interface TCSCopyFileController : NSObject {
    
}
@property (strong) NSString *sourceFilePath;

@property (assign)  id<TCSFileCopyDelegateProtocol> delegate;
- (void)copyFile:(NSString *)sourceFilePath toPath:(NSString *)destinationFilePath delegate:(id)inDelegate;
-(int)cancelCopyOperation;
@end
