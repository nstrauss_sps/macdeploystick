//
//  TCSUtility.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/2/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSUtility.h"
#include <sys/mount.h>
@implementation TCSUtility
+(NSArray *)listOfMedia{
    NSArray *mediaURLS= [[NSFileManager defaultManager]  mountedVolumeURLsIncludingResourceValuesForKeys:nil options:NSVolumeEnumerationSkipHiddenVolumes];

    NSMutableArray *paths=[NSMutableArray arrayWithCapacity:mediaURLS.count];
    [mediaURLS enumerateObjectsUsingBlock:^(NSURL *currFilesystemPath, NSUInteger idx, BOOL * _Nonnull stop) {
        struct statfs output;
        statfs([[currFilesystemPath path] UTF8String], &output);
        NSString *filesystemType=[NSString stringWithUTF8String:output.f_fstypename];
        if ([[currFilesystemPath path] isEqualToString: @"/"]==NO && [[filesystemType lowercaseString] containsString:@"hfs"]==YES) { //skip root
            [paths addObject:[currFilesystemPath path]];
        }
    }];
    return [NSArray arrayWithArray:paths];
}
@end
