//
//  TCSDefaultsViewController.m
//  MacDeployStick
//
//  Created by Timothy Perfitt on 1/5/19.
//  Copyright © 2019 Twocanoes Software. All rights reserved.
//

#import "TCSDefaultsViewController.h"

@interface TCSDefaultsViewController ()

@end

@implementation TCSDefaultsViewController
- (IBAction)restoreUserDefaultsButtonPressed:(id)sender {

    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];

}
- (IBAction)clearResourceCache:(id)sender {
    NSFileManager *fm=[NSFileManager defaultManager];
    NSArray *applicationSupportPath = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);

    NSString *appSupportDir=[applicationSupportPath[0] stringByAppendingPathComponent:@"com.twocanoes.macdeploystick"];

    if ([fm fileExistsAtPath:appSupportDir]) {

        NSError *err;
        if([fm removeItemAtPath:appSupportDir error:&err]==NO) {

            [[NSAlert alertWithError:err] runModal];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

@end
