//
//  TCDiskImageHelper.h
//  Winclone 3
//
//  Created by Timothy Perfitt on 3/1/12.
//  Copyright (c) 2012 Twocanoes Software Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TaskWrapper.h"
#define MOUNTING 1
#define UNMOUNTING 2

@protocol TCDiskImageHelperProtocol <NSObject>

- (void)diskImageMountedAtDevice:(NSString *)inDevice;
- (void)diskImageUnMounted;


@end
@interface TCDiskImageHelper : NSObject <TaskWrapperController>{
    TaskWrapper *tw;
    id delegate;
    NSMutableString *deviceInfo;
    int operation;
    
}
@property (strong) NSMutableString *deviceInfo;
-(NSString *)createdDiskImageWithSize:(NSString *)inSize filename:(NSString *)inFilename volumeName:(NSString *)inVolName;

-(void)attachDiskImageAtPath:(NSString *)inPath withDelegate:(id)sender;
-(void)unmountDiskImage:(NSString *)inPath withDelegate:(id)sender;

@end
